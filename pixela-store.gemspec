$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "pixela/store/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "pixela-store"
  s.version     = Pixela::Store::VERSION
  s.authors     = ["Jorge Escalante", "Estuardo Díaz", "Pablo Bautista"]
  s.email       = ["team@pixela.com.gt"]
  s.homepage    = "http://www.pixela.com.gt"
  s.summary     = "Pixela Store"
  s.description = "Pixela Store"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 4.2.6", ">= 4.2.6"
  s.add_dependency "devise", "~> 4.2.0", ">= 4.2.0"
  s.add_dependency "paperclip", "~> 5.0.0", ">= 5.0.0"
  s.add_dependency "aws-sdk", "~> 2.5.10", ">= 2.5.10"
  s.add_dependency "acts-as-taggable-on", "~> 4.0", ">= 4.0"
  s.add_dependency "rails-settings-cached", "~> 0.6.5", ">= 0.6.5"
  s.add_dependency "obfuscate_id", "~> 0.2.0", ">= 0.2.0"
  s.add_development_dependency "sqlite3"
end

class AddShippingToInventory < ActiveRecord::Migration
  def change
    add_reference :pixela_store_inventories, :shipping, index: true
  end
end

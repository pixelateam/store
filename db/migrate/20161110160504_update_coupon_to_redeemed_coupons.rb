class UpdateCouponToRedeemedCoupons < ActiveRecord::Migration
  def change
    remove_belongs_to :pixela_store_redeemed_coupons, :coupon_id
    add_reference :pixela_store_redeemed_coupons, :coupon, index: true
  end
end

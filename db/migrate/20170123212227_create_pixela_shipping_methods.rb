class CreatePixelaShippingMethods < ActiveRecord::Migration
  def change
    create_table :pixela_store_shipping_methods do |t|
      t.integer :shipping_type, null: false
      t.float :price, null: false
      t.integer :days
      t.belongs_to :shipping, index: true
      t.timestamps null: false
    end
  end
end

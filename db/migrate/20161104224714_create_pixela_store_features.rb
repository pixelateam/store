class CreatePixelaStoreFeatures < ActiveRecord::Migration
  def change
    create_table :pixela_store_features do |t|
      t.string :key, null: false
      t.belongs_to :feature_type, index: true
      t.boolean :is_default, null: false, default: false
      t.timestamps null: false
    end
  end
end

class AddColorToFeatures < ActiveRecord::Migration
  def change
    add_column :pixela_store_features, :color, :string
  end
end

class AddPrinceAndNameToInventory < ActiveRecord::Migration
  def change
    add_column :pixela_store_inventories, :price, :float, null: false
    add_column :pixela_store_inventories, :name, :string, null: false, default: ""
  end
end

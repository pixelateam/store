class CreatePixelaStoreRelatedProducts < ActiveRecord::Migration
  def change
    create_table :pixela_store_related_products do |t|
      t.belongs_to :related, index: true
      t.belongs_to :associated, index: true
    end
  end
end

class CreatePixelaStoreOrders < ActiveRecord::Migration
  def change
    create_table :pixela_store_orders do |t|
      t.belongs_to :customer, index: true
      t.belongs_to :payment_method, index: true
      t.belongs_to :shipping_address, index: true
      t.belongs_to :billing_address, index: true
      t.integer :status, default: 0
      t.integer :type, default: 0
      t.float :amount, null: false, default: 0.0
      t.float :shipping, null: false, default: 0.0
      t.float :taxes, null: false, default: 0.0
      t.float :discount, null: false, default: 0.0
      t.float :total_income, null: false, default: 0.0
      t.date :order_date
      t.string :observations
      t.string :provider
      t.string :provider_tracking
      t.timestamps null: false
    end
  end
end

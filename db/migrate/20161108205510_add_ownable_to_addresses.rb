class AddOwnableToAddresses < ActiveRecord::Migration
  def change
    add_column :pixela_store_addresses, :ownable_id, :integer
    add_column :pixela_store_addresses, :ownable_type, :string
    add_index :pixela_store_addresses, [:ownable_id, :ownable_type]
  end
end

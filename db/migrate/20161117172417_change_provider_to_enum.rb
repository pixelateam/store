class ChangeProviderToEnum < ActiveRecord::Migration
  def up
    remove_column :pixela_store_orders, :provider
    add_column :pixela_store_orders, :provider, :integer, default: 0, null: false
  end
  def down
    add_column :pixela_store_orders, :provider, :string
    remove_column :pixela_store_orders, :provider
  end
end

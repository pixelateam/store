class CreatePixelaShippings < ActiveRecord::Migration
  def change
    create_table :pixela_store_shippings do |t|
      t.string :name, null: false
      t.float :weight
      t.string :dimensions
      t.timestamps null: false
    end
  end
end

class AddDescriptionToPicture < ActiveRecord::Migration
  def change
    add_column :pixela_store_pictures, :description, :text
  end
end

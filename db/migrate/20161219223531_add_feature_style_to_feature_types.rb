class AddFeatureStyleToFeatureTypes < ActiveRecord::Migration
  def change
    add_column :pixela_store_feature_types, :feature_style, :integer, null: false, default: 0
  end
end

class CreatePixelaStoreOffices < ActiveRecord::Migration
  def change
    create_table :pixela_store_offices do |t|
      t.string :name, null: false, default: ""
      t.time :starting_time, null: false, default: '09:00'
      t.time :ending_time, null: false, default: '18:00'
      t.string :scheduling_rule
      t.timestamps null: false
    end
  end
end

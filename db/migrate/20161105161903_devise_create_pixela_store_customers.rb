class DeviseCreatePixelaStoreCustomers < ActiveRecord::Migration
  def change
    create_table :pixela_store_customers do |t|
      ## Customer profile
      t.string  :name,      null: false, default: ""
      t.string  :last_name, null: false, default: ""
      t.integer :gender,    null: false, default: 0
      t.date    :birth_date

      ## Customer payment profile
      t.string :payment_customer_id
      t.string :payment_provider

      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email

      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false
      t.string   :unlock_token
      t.datetime :locked_at


      t.timestamps null: false
    end

    add_index :pixela_store_customers, :email,                unique: true
    add_index :pixela_store_customers, :reset_password_token, unique: true
    add_index :pixela_store_customers, :confirmation_token,   unique: true
    add_index :pixela_store_customers, :unlock_token,         unique: true
  end
end

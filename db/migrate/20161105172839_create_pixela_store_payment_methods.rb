class CreatePixelaStorePaymentMethods < ActiveRecord::Migration
  def change
    create_table :pixela_store_payment_methods do |t|
      t.string      :provider,              null: false, default: ""
      t.string      :provider_customer_id
      t.string      :provider_customer_name
      t.integer     :type,                  null: false, default: 0
      t.belongs_to  :customer,              index: true
      t.integer     :expiration_month,      default: 1
      t.integer     :expiration_year,       default: 2000
      t.string      :provider_method_id,    default: ""
      t.string      :last4,                 default: "xxxx"
      t.string      :brand,                 default: ""
      t.boolean     :is_default,            null: false, default: false
      t.timestamps null: false
    end
  end
end

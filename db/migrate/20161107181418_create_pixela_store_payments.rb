class CreatePixelaStorePayments < ActiveRecord::Migration
  def change
    create_table :pixela_store_payments do |t|
      t.belongs_to :payment_method, index: true
      t.belongs_to :order, index: true
      t.integer  :status, default: 0
      t.float    :amount, default: 0.0
      t.float    :previous_balance, default: 0.0
      t.float    :current_balance, default: 0.0
      t.timestamps null: false
    end
  end
end

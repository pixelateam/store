class ChangeTypeColumnToPaymentMethods < ActiveRecord::Migration
  def change
    rename_column :pixela_store_payment_methods, :type, :payment_method_type
  end
end

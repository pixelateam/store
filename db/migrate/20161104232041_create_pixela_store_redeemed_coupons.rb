class CreatePixelaStoreRedeemedCoupons < ActiveRecord::Migration
  def change
    create_table :pixela_store_redeemed_coupons do |t|
      t.belongs_to  :coupon_id,         index: true
      t.integer     :redemption_count,  null: false, default: 0
      t.timestamps                      null: false
      t.timestamps null: false
    end
  end
end

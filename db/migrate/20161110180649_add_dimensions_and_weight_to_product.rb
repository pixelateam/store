class AddDimensionsAndWeightToProduct < ActiveRecord::Migration
  def change
    add_column :pixela_store_products, :dimensions, :string
    add_column :pixela_store_products, :weight, :float
  end
end

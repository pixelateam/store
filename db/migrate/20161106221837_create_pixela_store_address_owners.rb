class CreatePixelaStoreAddressOwners < ActiveRecord::Migration
  def change
    create_table :pixela_store_address_owners do |t|
      t.belongs_to :address, index: true
      t.references :ownable, polymorphic: true, index: { name: 'index_address_ownables' }
      t.timestamps null: false
    end
  end
end

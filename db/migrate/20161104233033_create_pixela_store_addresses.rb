class CreatePixelaStoreAddresses < ActiveRecord::Migration
  def change
    create_table :pixela_store_addresses do |t|
      t.string    :address_line_1,  null: false, default: ""
      t.string    :address_line_2
      t.string    :city,            null: false, default: ""
      t.string    :state,           null: false, defualt: ""
      t.string    :country,         null: false, default: ""
      t.string    :zip,             null: false, default: ""
      t.boolean   :is_default,      null: false, default: false
      t.string    :billing_first_name
      t.string    :billing_last_name
      t.string    :billing_email
      t.string    :phone_code
      t.string    :phone_number
      t.timestamps null: false
    end
  end
end

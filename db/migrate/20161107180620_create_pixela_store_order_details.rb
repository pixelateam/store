class CreatePixelaStoreOrderDetails < ActiveRecord::Migration
  def change
    create_table :pixela_store_order_details do |t|
      t.belongs_to :inventory, index: true
      t.belongs_to :order, index: true
      t.integer :quantity
      t.float :sub_total, default: 0
      t.float :discount, default: 0
      t.timestamps null: false
    end
  end
end

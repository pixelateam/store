class CreatePixelaStoreCoupons < ActiveRecord::Migration
  def change
    create_table :pixela_store_coupons do |t|
      t.string   :code,             null: false
      t.integer  :discount_percent
      t.float    :discount
      t.date     :expires_at,       null: false
      t.string   :description
      t.boolean  :free_shipping,    null: false, default: false
      t.integer  :max_redemptions,  null: false, default: -1
      t.timestamps null: false
    end
  end
end

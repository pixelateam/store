class AddLotAndLanToAddresses < ActiveRecord::Migration
  def change
    add_column :pixela_store_addresses, :longitude, :float
    add_column :pixela_store_addresses, :latitude, :float
  end
end

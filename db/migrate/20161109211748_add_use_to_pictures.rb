class AddUseToPictures < ActiveRecord::Migration
  def change
    add_column :pixela_store_pictures, :use, :integer, null: false, default: 0
  end
end

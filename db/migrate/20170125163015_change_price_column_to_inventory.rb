class ChangePriceColumnToInventory < ActiveRecord::Migration
  def change
    change_column :pixela_store_inventories, :price, :float, null: true
  end
end

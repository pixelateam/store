class CreatePixelaStoreFeatureTypes < ActiveRecord::Migration
  def change
    create_table :pixela_store_feature_types do |t|
      t.string :name, null: false, unique: true
      t.integer :sorting_type, default: 0, null: false
      t.string :sorting
    end
  end
end

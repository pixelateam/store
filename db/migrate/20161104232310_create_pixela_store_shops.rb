class CreatePixelaStoreShops < ActiveRecord::Migration
  def change
    create_table :pixela_store_shops do |t|
      t.belongs_to :product
      t.references :shopable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end

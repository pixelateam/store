class AddIsAvailableToProducts < ActiveRecord::Migration
  def change
    add_column :pixela_store_products, :available, :boolean
  end
end

class CreatePixelaStoreInventories < ActiveRecord::Migration
  def change
    create_table :pixela_store_inventories do |t|
      t.belongs_to :product, index: true
      t.integer :quantity, null: false, default: 0
      t.float :discount, default: 0.0
      t.integer :discount_percent, default: 0
      t.date :discount_availability
      t.timestamps null: false
    end
  end
end

class CreatePixelaProducts < ActiveRecord::Migration
  def change
    create_table :pixela_store_products do |t|
      t.string :name, null: false
      t.float  :price, null: false
      t.text :description, null: false
      t.boolean :best_seller, default: false
      t.boolean :available_for_preorder, default: false
      t.string :preorder_info
      t.date :preorder_due_date
      t.float :discount
      t.integer :discount_percent
      t.timestamps null: false
    end
  end
end

class CreatePixelaStoreFeatureLists < ActiveRecord::Migration
  def change
    create_table :pixela_store_feature_lists do |t|
      t.belongs_to :feature, index: true
      t.belongs_to :inventory, index: true
      t.timestamps null: false
    end
  end
end

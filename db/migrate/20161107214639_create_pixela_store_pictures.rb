class CreatePixelaStorePictures < ActiveRecord::Migration
  def change
    create_table :pixela_store_pictures do |t|
      t.attachment :data
      t.references :imageable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end

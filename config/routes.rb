Pixela::Store::Engine.routes.draw do
  devise_for :customers, class_name: "Pixela::Store::Customer", module: :devise
  devise_for :staffs, class_name: "Pixela::Store::Staff", module: :devise
end

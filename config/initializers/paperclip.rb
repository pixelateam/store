if Rails.env.production?
  Paperclip::Attachment.default_options.update({
    storage: :s3,
    s3_credentials: {
      bucket: ENV['S3_BUCKET_NAME'],
      access_key_id: ENV['S3_ACCESS_KEY'],
      secret_access_key: ENV['S3_SECRET_KEY'],
      s3_region: ENV['S3_BUCKET_REGION'],
    },
    path: "/:class/:style/:hash.:extension",
    default_url: "http://s3.amazonaws.com/#{ENV['S3_BUCKET_NAME']}/missing.jpg",
    hash_secret: Rails.application.secrets.secret_key_base
  })
else
  Paperclip::Attachment.default_options.update({
    storage: :filesystem,
    path: ":rails_root/public/media/:class/:style/:hash.:extension",
    url: "/media/:class/:style/:hash.:extension",
    default_url: "/media/missing.jpg",
    hash_secret: Rails.application.secrets.secret_key_base
  })
end

module Pixela
  module Store
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception

      before_filter :configure_permitted_parameters, if: :devise_controller?
      after_filter  :store_location

      protected

      def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :last_name, :email, :birth_date, :gender ,:password, :password_confirmation])
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :email, :birth_date, :gender ,:password, :password_confirmation])
      end

      def store_location
        return unless request.get?
        if (request.path != "/users/sign_in" &&
            request.path != "/users/sign_up" &&
            request.path != "/users/password/new" &&
            request.path != "/users/password/edit" &&
            request.path != "/users/confirmation" &&
            request.path != "/users/sign_out" &&
            !request.xhr?) # don't store ajax calls
          session[:previous_url] = request.fullpath
        else
          session[:previous_url] = new_user_session_path
        end
      end

      # protected
      # def after_sign_out_path_for(resource_or_scope)
      #   resource_or_scope == :staff ? new_staff_session_path : new_user_session_path
      # end
    end
  end
end

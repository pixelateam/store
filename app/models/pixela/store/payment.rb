# == Schema Information
# Table name: pixela_store_payments
#  id                     :integer          not_null, primary_key
#  payment_method_id      :integer          foreign_key
#  order_id               :integer          foreign_key
#  amount                 :float            default(0)
#  previous_balance       :float            default(0)
#  current_balance        :float            default(0)
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela
  module Store
    class Payment < ApplicationRecord
      belongs_to :payment_method
      belongs_to :order

      validates :amount, :previous_balance, :current_balance, numericality: { greater_or_equal_than: 0 }
      
    end
  end
end

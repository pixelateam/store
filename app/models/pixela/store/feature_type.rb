# == Schema Information
# Table name: pixela_store_feature_types
#  id                     :integer          not_null, primary_key
#  name                   :string           not_null, unique
#  sorting_type           :integer          not_null, default(0)
#  sorting                :string
#
module Pixela
  module Store
    class FeatureType < ApplicationRecord
      include Linkeable
      obfuscate_id
      
      enum sorting_type: [:ascendent, :descendent, :custom]
      enum feature_style: [:colors, :texts, :sizes, :texts_and_images]
      validates :name, presence: true, uniqueness: true

      has_many :features

      accepts_nested_attributes_for :features, allow_destroy: true
    end
  end
end

# == Schema Information
# Table name: pixela_store_shipping_methods
#  id                     :integer          not_null, primary_key
#  type                   :integer
#  price                  :float
#  days                   :integer
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela::Store
  class ShippingMethod < ApplicationRecord
    belongs_to :package

    enum shipping_type: [:local, :economic, :standard]
    validates :shipping_type, uniqueness: true

    validates_presence_of :shipping_type, :price, :days
  end
end

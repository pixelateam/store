module Pixela
  module Store
    class Staff < ApplicationRecord
      devise :database_authenticatable,
             :recoverable, :rememberable, :trackable, :validatable,
             :registerable
    end
  end
end

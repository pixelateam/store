# == Schema Information
# Table name: pixela_store_shops
#  id                     :integer          not_null, primary_key
#  product_id             :integer          not_null, foreign_key
#  shopable_id            :integer          foreign_key
#  shopable_type          :string           default("")
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#

module Pixela
  module Store
    class Shop < ApplicationRecord
      belongs_to :shopable, polymorphic: true
      belongs_to :product

      validates_uniqueness_of :product, scope: [:shopable]
    end
  end
end

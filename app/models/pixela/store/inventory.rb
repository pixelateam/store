# == Schema Information
# Table name: pixela_store_inventories
#  id                     :integer          not_null, primary_key
#  product_id             :integer          not_null, foreign_key
#  name                   :string           not_null, default('')
#  price                  :float            not_null
#  quantity               :integer
#  discount               :float            default(0)
#  discount_percent       :integer          default(0)
#  discount_availability  :date
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela
  module Store
    class Inventory < ApplicationRecord
      obfuscate_id

      attr_accessor :feature_type, :feature
      belongs_to :product
      belongs_to :shipping
      validates_presence_of :name
      validates :quantity, numericality: { greater_or_equal_than: 0 }, presence: true

      has_many :pictures, as: :imageable
      validates :pictures, length: { minimum: 1 }
      accepts_nested_attributes_for :pictures, allow_destroy: true

      has_many :feature_lists
      has_many :features, through: :feature_lists

      accepts_nested_attributes_for :feature_lists, allow_destroy: true

      def total_discount
        total_discount = 0
        total_discount = self.price * self.discount_percent if self.discount_percent
        total_discount += self.discount if self.discount
        return total_discount
      end

    end
  end
end

# == Schema Information
# Table name: pixela_store_addresses
#  id                 :integer          not_null, primary_key
#  address_line_1     :string           not_null, default("")
#  address_line_2     :string
#  city               :string           not_null, default("")
#  state              :string           not_null, default("")
#  country            :string           not_null, default("")
#  zip                :string           not_null, default("")
#  longitude          :float
#  latitude           :float
#  is_default         :boolean          not_null, default(false)
#  billing_first_name :string
#  billing_last_name  :string
#  billing_email      :string
#  phone_code         :string
#  phone_number       :string
#  ownable_id         :integer
#  ownable_type       :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module Pixela::Store
  class Address < ApplicationRecord
    obfuscate_id

    validates_presence_of :address_line_1, :city, :state, :country, :zip
    validates_inclusion_of :is_default, in: [true, false]

    belongs_to :ownable, polymorphic: true

    def display_name
      "#{billing_first_name} #{billing_last_name}"
    end

    def to_s
      "#{address_line_1} #{address_line_2}, #{city}, #{state} #{zip}, #{country}"
    end
  end
end

# == Schema Information
# Table name: pixela_store_related_products
#  id                     :integer          not_null, primary_key
#  related_id             :integer          not_null, primary_key
#  associated_id          :integer          not_null, primary_key
#
module Pixela
  module Store
    class RelatedProduct < ApplicationRecord
      belongs_to :related, class_name: "Product", foreign_key: "related_id"
      belongs_to :associated, class_name: "Product", foreign_key: "associated_id"

      # scope :related_to, -> (product_id) { where("associated_id = :product_id OR related_id=:product_id ", {product_id: product_id}) }
    end
  end
end

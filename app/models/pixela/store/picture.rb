# == Schema Information
# Table name: pixela_store_pictures
#  id                 :integer          not_null, primary_key
#  data_file_name     :string
#  data_content_type  :string
#  data_file_size     :integer
#  data_updated_at    :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  created_at         :datetime         not_null
#  updated_at         :datetime         not_null
#  description        :text
#

module Pixela::Store
  class Picture < ApplicationRecord
    belongs_to :imageable, polymorphic: true

    has_attached_file :data,
                      styles: {large: "680x680>",medium: "300x300>", thumb: "100x100>"}
    validates_attachment_content_type :data, content_type:  ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/svg+xml"]
    validates_with AttachmentSizeValidator, attributes: :data, less_than: 30.megabytes

    validates_attachment :data, attachment_presence: true

    enum use: [:front, :rear, :other, :collection, :color_swatch]
    def file_extension
      "#{self.data_file_name.split('.').last}"
    end

    def url
      "#{ENV['DOMAIN']}#{self.data.url}"
    end
  end
end

# == Schema Information
# Table name: pixela_store_coupons
#  id                 :integer          not_null, primary_key
#  code               :string           not_null
#  discount           :float
#  discount_percent   :integer
#  expires_at         :date
#  description        :string
#  free_shipping      :boolean          not_null, default(false)
#  max_redemptions    :integer          not_null, default(-1)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module Pixela::Store
  class Coupon < ApplicationRecord
    validates_presence_of :code, :max_redemptions, :expires_at
    validates_uniqueness_of :code
    validates :max_redemptions, numericality: { only_integer: true, greater_than_or_equal_to: -1 }
    validates_inclusion_of :free_shipping, in: [true, false]

    has_many :redeemed_coupons
  end
end

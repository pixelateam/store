# == Schema Information
# Table name: pixela_store_products
#  id                     :integer          not_null, primary_key
#  name                   :string           not_null
#  price                  :float            not_null
#  description            :text             not_null
#  best_seller            :boolean          default(false)
#  available_for_preorder :boolean          default(false)
#  preorder_info          :string
#  preorder_due_date      :date
#  discount               :float
#  discount_percent       :integer
#  weight                 :float
#  dimension              :string
#  product_care           :text
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#  available              :boolean
#
module Pixela
  module Store
    class Product < ApplicationRecord
      include Linkeable
      obfuscate_id

      validates_presence_of :name, :price, :description
      # validates :price, :discount, :discount_percent, numericality: { greater_than: 0.0 }
      has_many :inventories
      has_many :pictures, through: :inventories
      has_many :features, through: :inventories
      has_many :feature_types, through: :features

      # has_many :related_products, foreign_key: :associated_id
      # has_many :related_to, through: :related_products, source: :related
      has_many :related_product_associations, foreign_key: :related_id, class_name: 'RelatedProduct'
      has_many :related_products, through: :related_product_associations, source: :associated
      has_many :associated_product_associations, foreign_key: :associated_id, class_name: 'RelatedProduct'
      has_many :associated_products, through: :associated_product_associations, source: :related

      has_many :shops
      acts_as_taggable_on :categories

      def total_discount
        total_discount = 0
        total_discount = self.price * self.discount_percent if self.discount_percent
        total_discount += self.discount if self.discount
        return total_discount
      end

      def related_to
        (associated_products + related_products).flatten.uniq
      end

      def to_s
        name
      end

      def display_name
        self.to_s
      end
    end
  end
end

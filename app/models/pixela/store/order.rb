# == Schema Information
# Table name: pixela_store_orders
#  id                     :integer          not_null, primary_key
#  customer_id            :integer          foreign_key
#  payment_method_id      :integer          foreign_key
#  shipping_address_id    :integer          foreign_key
#  billing_address_id     :integer          foreign_key
#  status                 :integer          default(0)
#  amount                 :float            not_null, default(0)
#  shipping               :float            not_null, default(0)
#  taxes                  :float            not_null, default(0)
#  discount               :float            not_null, default(0)
#  total_income           :float            not_null, default(0)
#  order_date             :date
#  observations           :string
#  provider_type          :integer          not_null, default(0)
#  provider_tracking      :string
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela
  module Store
    class Order < ApplicationRecord
      belongs_to :customer
      belongs_to :payment_method
      belongs_to :billing_address, class_name: "Address"
      belongs_to :shipping_address, class_name: "Address"

      enum provider: [:isse_order, :isse_preorder, :amazon]
    end
  end
end

# == Schema Information
# Table name: pixela_store_orders
#  id                     :integer          not_null, primary_key
#  inventory_id           :integer          foreign_key
#  order_id               :integer          foreign_key
#  quantity               :integer
#  sub_total              :float            default(0)
#  discount               :float            default(0)
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela
  module Store
    class OrderDetail < ApplicationRecord
      belongs_to :inventory
      belongs_to :order
    end
  end
end

# == Schema Information
# Table name: users
#  id                     :integer          not_null,primary_key
#  name                   :string           not_null, default("")
#  last_name              :string           not_null, default("")
#  gender                 :integer          not_null, default(0)
#  birth_date             :date
#  payment_customer_id    :string
#  payment_provider       :string
#  email                  :string           not_null, default("")
#  encrypted_password     :string           not_null, default("")
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          not_null, default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmation_at        :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          not_null, default(0)
#  unlock_token           :string
#  locked_at              :string
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
require 'stripe'

module Pixela::Store
  class Customer < ApplicationRecord
    devise :database_authenticatable,
           :recoverable, :rememberable, :trackable, :validatable,
           :registerable, :confirmable, :lockable
    enum gender: [:male, :female]
    validates_presence_of :name, :last_name, :gender
    has_many :payment_methods
    has_many :addresses, as: :ownable

    def to_s
      full_name
    end

    def full_name
      "#{name} #{last_name}"
    end

    def display_name
      "#{id}- #{name} #{last_name}"
    end

    def as_customer
      if payment_customer_id == "" || payment_customer_id == nil
        customer = Stripe::Customer.create(description: "Customer for #{email}", email: email)
        update(payment_customer_id: customer[:id], payment_provider: 'stripe')
      else
        customer = Stripe::Customer.retrieve(payment_customer_id)
      end
      customer
    end

    def get_payment_method(method_id)
      customer = as_customer
      customer.sources.retrieve(method_id)
    end

    def send_confirmation_instructions

    end

    def send_reset_password_instructions_notification(token)

    end
  end
end

# == Schema Information
# Table name: pixela_store_redeemed_coupons
#  id                 :integer          not_null, primary_key
#  coupon_id          :integer          not_null, foreign_key
#  redemption_count   :integer          default(0), not_null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module Pixela::Store
  class RedeemedCoupon < ApplicationRecord
    validates_presence_of :redemption_count
    belongs_to :coupon
  end
end

# == Schema Information
# Table name: pixela_store_offices
#  id               :integer          not_null, primary_key
#  name             :string           not_null, default("")
#  starting_time    :time             not_null, default("09:00")
#  ending_time      :time             not_null, default("18:00")
#  scheduling_rule  :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

module Pixela::Store
  class Office < ActiveRecord::Base
    has_one :address, as: :ownable
    has_many :pictures, as: :imageable
    accepts_nested_attributes_for :address, allow_destroy: true
    accepts_nested_attributes_for :pictures, allow_destroy: true

    def to_s
      name
    end

    def display_name
      name
    end
  end
end

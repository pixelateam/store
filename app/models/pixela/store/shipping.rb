# == Schema Information
# Table name: pixela_store_shippings
#  id                     :integer          not_null, primary_key
#  name                   :string
#  weight                 :float
#  dimensions             :string
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela::Store
  class Shipping < ApplicationRecord
    validates_presence_of :name

    has_many :inventories
    has_many :shipping_methods
    accepts_nested_attributes_for :shipping_methods, allow_destroy: true
  end
end

# == Schema Information
# Table name: pixela_store_payment_methods
#  id                     :integer          not_null, primary_key
#  provider               :string           not_null, default("")
#  provider_customer_id   :string
#  provider_customer_name :string
#  type                   :integer          not_null, default(false)
#  customer_id            :integer          not_null, foreign_key
#  expiration_month       :integer          default(1)
#  expiration_year        :integer          default(2000)
#  provider_method_id     :string           default("")
#  last4                  :string           default("xxxx")
#  brand                  :string           default("")
#  is_default             :boolean          not_null, default(false)
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#

module Pixela::Store
  class PaymentMethod < ActiveRecord::Base
    obfuscate_id

    enum type: [:credit_card, :debit_card, :paypal, :google_wallet]
    validates_presence_of :customer_id, :provider

    belongs_to :customer
    has_many :purchases

    def save_from_stripe(stripe_card)
      params = {provider: 'stripe'}
      params = params.merge( stripe_card.slice(:last4, :brand, :default) )
      params[:provider_method_id] = stripe_card[:id]
      assign_attributes(params)
      save()
    end

    def to_s
      answer = provider_customer_id.to_s + ' - ' + provider
      return answer
    end
  end
end

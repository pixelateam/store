# == Schema Information
# Table name: pixela_store_payment_methods
#  id                     :integer          not_null, primary_key
#  feature_id             :integer          not_null
#  inventory_id           :integer
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela
  module Store
    class FeatureList < ApplicationRecord
      attr_accessor :feature_type, :new_feature
      belongs_to :feature
      belongs_to :inventory

      scope :by_feature_type, ->(feature_type_id, inventory_id) { includes(:feature).where(pixela_store_features: { feature_type_id: feature_type_id }, inventory_id: inventory_id)}
    end
  end
end

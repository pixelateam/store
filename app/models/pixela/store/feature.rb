# == Schema Information
# Table name: pixela_store_features
#  id                     :integer          not_null, primary_key
#  key                    :string           not_null
#  feature_type_id        :integer
#  is_default             :boolean          not_null, default(false)
#  created_at             :datetime         not_null
#  updated_at             :datetime         not_null
#
module Pixela
  module Store
    class Feature < ApplicationRecord
      belongs_to :feature_type
      validates :key, presence: true, uniqueness: { scope: :feature_type}

      def to_s
        key
      end
    end
  end
end

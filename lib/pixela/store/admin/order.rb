ActiveAdmin.register Pixela::Store::Order, as: "Order" do
  index do
    selectable_column
    id_column
    column :customer
  end
end

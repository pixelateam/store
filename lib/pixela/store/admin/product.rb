ActiveAdmin.register Pixela::Store::Product, as: "Product" do
  permit_params :name, :description, :price, :best_seller, :available_for_preorder,
                :preorder_info, :preorder_due_date, :discount, :discount_percent,
                :dimensions, :weight, :product_care, :category_list

  inventory_name = Pixela::Store::Inventory.model_name.human

  index do
    selectable_column
    id_column
    column :name
    column :presentations do |product|
      ul do
        product.inventories.each do |inventory|
          li link_to inventory.name, admin_product_inventory_path(product, inventory)
        end
      end
    end
    column :stock do |product|
      product.inventories.sum(:quantity)
    end
    column :available_for_preorder
    column :best_seller
    actions
  end
  show do
    attributes_table do
      row :id
      row :name
      row :description
      row :product_care
      row :category_list
      row :price
      row :discount
      row :discount_percent
      row :dimensions
      row :weight
      row :best_seller
      row :available_for_preorder
      row :created_at
      row :updated_at
      row :related_products do |product|
        div class: :gallery do
          product.related_to.each do |related_product|
            if related_product.pictures.length > 0
              div class: :image do
                link_to admin_product_path(related_product) do
                  image_tag(related_product.pictures.first.data.url(:thumb))
                end
              end
            else
              div class: :image do
                link_to admin_product_path(related_product) do
                  image_tag(Pixela::Store::Picture.find(Pixela::Store::Setting[:default_product_picture_id]).data.url(:thumb))
                end
              end
            end
          end
        end
      end
    end
  end

  form do |f|
    f.inputs "product_details" do
      f.input :name
      f.input :description
      f.input :product_care
      f.input :category_list, hint: 'Comma separated', label: :categories, input_html: {value: f.object.category_list.to_s }
      f.input :dimensions
      f.input :weight
      f.input :best_seller
    end

    f.inputs :price_details do
      f.input :price
      f.input :discount
      f.input :discount_percent
    end

    f.inputs :preorder_details do
      f.input :preorder_info
      f.input :preorder_due_date, as: :datepicker
      f.input :available_for_preorder
    end
    unless f.object.new_record?
      f.inputs :related_products do
        f.input :related_to, as: :check_boxes, collection: Pixela::Store::Product.tagged_with(product.category_list, on: :categories, any: true) - [product], checked: product.related_to.collect(&:id)
      end
    end

    f.actions
  end

  sidebar inventory_name.pluralize, only: [:show] do
    if product.inventories.length > 0
      table_for product.inventories do |t|
        t.column(:name) { |inventory| link_to inventory.name, admin_product_inventory_path(product, inventory) }
        t.column(:stock) { |inventory| inventory.quantity }
      end
    else
      h4 t("sidebar_table.empty", resource: inventory_name.pluralize)
    end
    link_to t("sidebar_table.new", resource: inventory_name), new_admin_product_inventory_path(product)
  end

  sidebar :preorder_info, only: [:show], if: proc{ product.available_for_preorder } do
    b "Description:"
    para product.preorder_info
    b "Due date:"
    span product.preorder_due_date.strftime("%D")
    table_for product.inventories do |t|
      t.column(:Order) { }
      t.column(:actions) { }
    end
  end

  controller do
    def update
      @product = Pixela::Store::Product.find(params[:id])
      related_ids = params[:product][:related_to].reject(&:blank?).map(&:to_i)
      deleted_related_ids = (
        @product.related_to.collect(&:id) - related_ids
      )
      added_related_ids = (
        related_ids - @product.related_to.collect(&:id)
      )
      added_related_ids.each do |related_id|
        Pixela::Store::RelatedProduct.create(
          associated_id: @product.id,
          related_id: related_id
        ) unless Pixela::Store::RelatedProduct.exists?(
          associated_id: @product.id,
          related_id: related_id
        )
      end
      if @product.update(permitted_params[:product])
        deleted_related_ids.each do |related_id|
          ids = [@product.id, related_id]
          Pixela::Store::RelatedProduct.where(
            associated_id: ids,
            related_id: ids
          ).destroy_all
        end
        redirect_to admin_product_path(@product)
      else
        flash[:error] ||= []
        flash[:error].concat(@product.errors.full_messages)
        redirect_to :back
      end
    end
  end
end

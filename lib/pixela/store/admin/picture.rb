ActiveAdmin.register Pixela::Store::Picture, as: "Picture" do
  permit_params :use, :data

  index do
    selectable_column
    id_column
    column :data_file_name
    column :file_size do |picture|
      number_to_human_size(picture.data_file_size, precision: 2)
    end
    column :data_updated_at
    column :image do |picture|
      image_tag picture.data.url(:thumb)
    end
    actions
  end

  show do |picture|
    attributes_table do
      row :data_file_name
      row :model do |picture|
        link_to picture.imageable.to_s, send("admin_#{picture.imageable.class.to_s.downcase.demodulize}_path", picture.imageable) if picture.imageable
      end
      row :file_size do |picture|
        number_to_human_size(picture.data_file_size, precision: 2)
      end
      row :use
      row :created_at
      row :updated_at
      row "Picture" do
        div do
          image_tag(picture.data.url(:medium))
        end
      end
    end
  end

  form do |f|
    f.inputs "Picture details" do
      f.input :use
      f.input :data, as: :file, hint: image_tag(f.object.data.url(:medium))
    end
    f.actions
  end
end

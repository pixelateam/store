ActiveAdmin.register Pixela::Store::Customer, as: "Customer" do
  payment_method_name = Pixela::Store::PaymentMethod.model_name.human
  address_name = Pixela::Store::Address.model_name.human
  index do
    selectable_column
    id_column
    column :full_name
    column :email
    column :gender
    column :orders do
      0
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :full_name
      row :email
      row :gender
      row :birth_date
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs :customer_details do
      f.input :name
      f.input :last_name
      f.input :gender
      f.input :birth_date, as: :datepicker
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
  end

  sidebar payment_method_name.pluralize, only: [:show] do
    if customer.payment_methods.length > 0
      table_for customer.payment_methods do |t|
        t.column(:provider) { |payment_method| payment_method.provider }
        t.column(:brand) { |payment_method| payment_method.brand }
        t.column(:type) { |payment_method| payment_method.last4 }
      end
    else
      h4 t("sidebar_table.empty", resource: payment_method_name.pluralize)
    end
    link_to t("sidebar_table.new", resource: payment_method_name), new_admin_customer_address_path(customer.id)
  end

  sidebar address_name.pluralize, only: [:show] do
    if customer.addresses.length > 0
      table_for customer.addresses do |t|
        t.column(t(:address)) { |address| address }
        t.column(:actions) { |address| link_to t(:view), admin_customer_address_path(customer, address) }
      end
    else
      h4 t("sidebar_table.empty", resource: address_name.pluralize)
    end
    link_to t("sidebar_table.new", resource: address_name), new_admin_customer_address_path(customer.id)
  end
end

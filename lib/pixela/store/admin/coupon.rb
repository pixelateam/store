ActiveAdmin.register Pixela::Store::Coupon, as: "Coupon" do
  permit_params :code, :description, :discount, :discount_percent, :expires_at,
                :free_shipping, :max_redemptions

  index do
    selectable_column
    id_column
    column :code
    column :description
    column :expires_at
    column :max_redemptions
    actions
  end

  show do
    attributes_table do
      row :id
      row :code
      row :description
      row :max_redemptions
      row :expires_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs :coupon_details do
      f.input :code
      f.input :description
      f.input :discount
      f.input :discount_percent
      f.input :expires_at, as: :datepicker
      f.input :free_shipping
      f.input :max_redemptions
    end
    f.actions
  end

  sidebar "Redemptions", only: [:show] do
    coupon = Pixela::Store::Coupon.find(params[:id])
    table_for coupon.redeemed_coupons do |t|
      t.column(:customer) { |redeem| redeem.customer ? redeem.customer.display_name : 'Anonymous user' }
      t.column(:date) { |redeem| redeem.created_at }
    end
  end
end

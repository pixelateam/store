ActiveAdmin.register Pixela::Store::Office, as: "Office" do
  permit_params :name, :starting_time, :ending_time,
                address_attributes: [:address_line_1, :address_line_2, :city,
                :state, :country, :zip, :phone_code, :phone_number],
                pictures_attributes: [:id, :data, :_destroy]

  index do
    selectable_column
    id_column
    column :name
    column :address do |office|
      "#{office.address.to_s}"
    end
    column :hours do |office|
      "#{office.starting_time.strftime('%R')}-#{office.ending_time.strftime('%R')}"
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :address do |office|
        office.address.to_s
      end
      row :hours do |office|
        "#{office.starting_time.strftime('%R')}-#{office.ending_time.strftime('%R')}"
      end
      row :created_at
      row :updated_at
    end
  end


  form do |f|
    f.inputs :office_details do
      f.input :name
      f.input :starting_time, as: :time_picker
      f.input :ending_time, as: :time_picker
    end

    f.inputs :address_details, for: [:address, f.object.address || Pixela::Store::Address.new] do |a|
      a.input :address_line_1
      a.input :address_line_2
      a.input :city
      a.input :state
      a.input :zip
      a.input :country, priority_countries: [:gt, :us]
    end

    f.inputs "Gallery" do
      f.has_many :pictures do |picture|
        picture.input :use
        picture.input :data, as: :file, hint: image_tag(picture.object.data.url(:medium))
        if picture.object.new_record? == false
          picture.input :_destroy, as: :boolean, required: :false, label: 'Remove'
        end
      end
    end

    f.actions
  end
end

ActiveAdmin.register_page "Social" do

  content do
    render partial: 'form'
  end

  page_action :update, method: :post do
    Pixela::Store::Setting.og_title = params[:title]
    Pixela::Store::Setting.og_description = params[:description]
    picture = Pixela::Store::Picture.new
    default_product_picture = Pixela::Store::Picture.new
    if params[:image]
      picture.data = params[:image]
      if picture.save
        Pixela::Store::Setting.og_picture_id = picture.id
      end
    end
    if params[:default_product_picture]
      default_product_picture.data = params[:default_product_picture]
      if default_product_picture.save
        Pixela::Store::Setting.default_product_picture_id = default_product_picture.id
      end
    end
    redirect_to admin_social_path
  end
end

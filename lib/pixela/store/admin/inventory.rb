ActiveAdmin.register Pixela::Store::Inventory, as: "Inventory" do
  permit_params :name, :quantity, :price, :discount, :discount_percent,
                pictures_attributes: [:id, :data, :_destroy]

  actions :all, except: [:index]

  belongs_to :product, polymorphic: true
  member_action :change_features, method: :post
  config.filters = false
  index do
    selectable_column
    id_column
    column :name
    column :price
    column :quantity
    actions
  end
  show do
    attributes_table do
      row :id
      row :name
      row :price
      row :discount
      row :discount_percent
      row :quantity
      row :pictures do |inventory|
        div class: :gallery do
          inventory.pictures.each do |img|
            div class: :image do
              image_tag(img.data.url(:thumb))
            end
          end
        end
      end
    end
  end

  form do |f|
    f.inputs :inventory_details do
      f.input :name
      f.input :quantity
      f.input :price
      f.input :discount
      f.input :discount_percent
    end
    f.inputs :features do
      # f.input :feature_type, as: :select, collection: Pixela::Store::FeatureType.all, input_html: {
      #   onchange: remote_request(:post, :change_features, {feature_type_id: "$('#inventory_feature_type').val()", input_id: "this.id"}, :inventory_feature)
      # }
      # f.input :feature, as: :select

      f.has_many :feature_lists do |feature|
        if feature.object.new_record?
          feature.input :feature_type, as: :select, collection: Pixela::Store::FeatureType.all, input_html: {
            onchange: remote_request(:post, :change_features, { feature_type_id: "this.value", input_id: "this.id" })
          }
          feature.input :new_feature, as: :select
        else
          feature.label h3 "#{feature.object.feature.feature_type.name}: #{feature.object.feature.key}"
          feature.input :_destroy, as: :boolean, required: :false, label: 'Remove'
        end
      end
    end
    f.inputs :gallery do
      f.has_many :pictures do |picture|
        picture.input :use
        picture.input :data, as: :file, hint: image_tag(picture.object.data.url(:medium))
        if picture.object.new_record? == false
          picture.input :_destroy, as: :boolean, required: :false, label: 'Remove'
        end
      end
    end
    f.actions
  end

  controller do
    def index
      @product = Pixela::Store::Product.find(params[:product_id]) if params[:product_id]
      @inventories = @product.inventories.page(params[:page]).per(20)
      @inventory = Pixela::Store::Inventory.new
    end
    def show
      @product = Pixela::Store::Product.find(params[:product_id])
    end

    def new
      @product = Pixela::Store::Product.find(params[:product_id])
      @inventory = Pixela::Store::Inventory.new
    end

    def edit
      @product = Pixela::Store::Product.find(params[:product_id])
      @inventory = Pixela::Store::Inventory.find(params[:id])
    end

    def create
      @product = Pixela::Store::Product.find(params[:product_id])
      @inventory = Pixela::Store::Inventory.new(permitted_params[:inventory])
      @inventory.product = @product

      if @inventory.save
        redirect_to admin_product_inventory_path(@product, @inventory)
      else
        flash[:error] ||= []
        flash[:error].concat(@inventory.errors.full_messages)
        redirect_to :back
      end
    end

    def update
      @product = Pixela::Store::Product.find(params[:product_id])
      @inventory = Pixela::Store::Inventory.find(params[:id])
      @inventory.update_attributes(permitted_params[:inventory])
      if @inventory.save
        params[:inventory][:feature_lists_attributes].values.each do |feature_list|
          features = Pixela::Store::FeatureList.by_feature_type(feature_list[:feature_type])
          features.destroy_all if features.where(feature: feature_list[:new_feature]).length == 0
          Pixela::Store::FeatureList.create(feature_id: feature_list[:new_feature], inventory: @inventory) if feature_list[:new_feature]
          Pixela::Store::FeatureList.destroy(feature_list[:id]) if feature_list[:_destroy] == '1'
        end
        redirect_to admin_product_inventory_path(@product, @inventory)
      else
        flash[:error] ||= []
        flash[:error].concat(@inventory.errors.full_messages)
        redirect_to :back
      end
    end

    def change_features
      input_id = params[:input_id].sub! 'feature_type', 'new_feature'
      @features = Pixela::Store::Feature.where(feature_type_id: params[:feature_type_id])
      render json: {input_id: input_id, text: view_context.options_from_collection_for_select(@features, :id, :key)}
    end
  end
end

ActiveAdmin.register Pixela::Store::FeatureType, as: "FeatureType" do
  permit_params :name, :sorting_type, :sorting,
                features_attributes: [:id, :key, :_destroy]
  index do
    selectable_column
    id_column
    column :name
    column :features do |feature_type|
      feature_type.features.join(", ")
    end
    column :order do |feature_type|
      if feature_type.ascendent? || feature_type.descendent?
        feature_type.sorting_type
      else
        feature_type.sorting
      end
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :sorting_type
      row :sorting
      row :features do |feature_type|
        feature_type.features.join(", ")
      end
    end
  end

  form do |f|
    f.inputs :feature_type_details do
      f.input :name
      f.input :sorting_type
      f.input :sorting
    end

    f.inputs :features do
      f.has_many :features do |feature|
        feature.input :key
        if feature.object.new_record? == false
          feature.input :_destroy, as: :boolean, required: :false, label: 'Remove'
        end
      end
    end

    f.actions
  end
end

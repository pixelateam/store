ActiveAdmin.register Pixela::Store::Address, as: "Address" do
  permit_params :address_line_1, :address_line_2, :city, :state, :country, :zip,
                :longitude, :latitude, :billing_first_name, :billing_last_name,
                :billing_email, :phone_code, :phone_number

  belongs_to :customer, polymorphic: true
  index do
    selectable_column
    id_column
    column :address do |address|
      "#{address.address_line_1} #{address.address_line_2}"
    end
    column :city
    column :state
    column :country
    actions
  end

  show do
    attributes_table do
      row :id
      row :full_name do |address|
        "#{address.billing_first_name} #{address.billing_last_name}"
      end
      row :address_line_1
      row :address_line_2
      row :city
      row :state
      row :zip
      row :country
      row :phone do |address|
        "+#{address.phone_code} #{address.phone_number}"
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs :address_details do
      f.input :address_line_1
      f.input :address_line_2
      f.input :city
      f.input :state
      f.input :zip
      f.input :country, priority_countries: [:gt, :us]
    end

    f.inputs :phone_details do
      f.input :phone_code
      f.input :phone_number
    end

    f.inputs :billing_details do
      f.input :billing_first_name
      f.input :billing_last_name
      f.input :billing_email
    end

    f.inputs :geo_location_details do
      f.input :longitude
      f.input :latitude
    end

    f.actions
  end

  sidebar :owner, only: [:show] do
    address = Pixela::Store::Address.find(params[:id])
    if address.ownable_type == Pixela::Store::Customer.name
      attributes_table_for address.ownable do
        row :id
        row :display_name
        row :email
      end
    elsif address.ownable_type == Pixela::Store::Office.name
      attributes_table_for address.ownable do
        row :id
        row :name
      end
    end
    object_name = address.ownable.model_name.param_key
    helper_ = "admin_#{object_name}_url"
    url = Rails.application.routes.url_helpers.send(helper_, address.ownable) if address.ownable
    link_to t("sidebar_table.detail", resource: address.ownable.model_name.human), url
  end

  controller do
    belongs_to :customer,polymorphic: true
    def show
      @customer = Pixela::Store::Customer.find(params[:customer_id])
    end

    def new
      @customer = Pixela::Store::Customer.find(params[:customer_id])
      @address = Pixela::Store::Address.new
    end

    def create
      @customer = Pixela::Store::Customer.find(params[:customer_id])
      @address = Pixela::Store::Address.new(permitted_params[:address])
      @address.ownable = @customer

      if @address.save
        redirect_to admin_customer_path(@customer)
      else
        flash[:error] ||= []
        flash[:error].concat(@address.errors.full_messages)
        redirect_to :back
      end
    end
  end
end
